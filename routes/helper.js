const jwt = require('jsonwebtoken');
module.exports.authenticateToken = function(req , res , next){
    // console.log(req.headers.authorization);
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(' ')[1];
    //console.log(authHeader);
    if(token == null) return res.status(401).json({ success : false , message : "Doesn't exist token"});
  
    jwt.verify(token , process.env.ACCESS_TOKEN_SECRET , (err,user) => {
      if(err) return res.status(403).json({ success : false , message : "Token isn't valid"});
      req.user = user;
      next();
    })
}