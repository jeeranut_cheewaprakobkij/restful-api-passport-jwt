const express = require('express');
const router = express.Router();

const helper = require('./helper');

// Import Model
const Book = require('../models/book');

// Import Controller
const bookController = require('../controllers/book');

router.get('/' , helper.authenticateToken , bookController.getBook );

router.get('/:_id' , helper.authenticateToken , bookController.getBookById );

router.post('/' , bookController.addBook );

router.put('/:_id' , bookController.updateBook );

router.delete('/:_id' , bookController.deleteBook );

module.exports = router;
