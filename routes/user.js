const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/keys');
require('dotenv').config();
// Load User model
const User = require('../models/User');

// Register
router.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];
    console.log(`name : ${name} , email : ${email} , password : ${password} , password2 : ${password2}`)

    if (!name || !email || !password || !password2) {
        //console.log('Error Please enter all fields');
        errors.push({ msg: 'Please enter all fields' });
        return res.status(400).json({ success : false , msg : "Please enter all fields" });
    }
  
    if (password != password2) {
        //console.log("password doest not match");
        errors.push({ msg: 'Passwords do not match' });
        return res.status(400).json({ success : false , msg : "Password do not match" });
    }
    
    if (password.length < 6) {
      errors.push({ msg: 'Password must be at least 6 characters' });
      return res.status(400).json({ success : false , msg : "Password must be at least 6 characters"});
    }
  
    User.findOne({ email: email }).then(user => {
        if (user) {
          errors.push({ msg: 'Email already exists' });
          return res.status(400).json({ success : false , msg : "Email already exist"});
        //   res.render('register', {
        //     errors,
        //     name,
        //     email,
        //     password,
        //     password2
        //   });
        } else {
          const newUser = new User({
            name,
            email,
            password
          });
  
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, 
                // Callback function
                function(err, hash) {
                if (err) throw err;
                newUser.password = hash;
                newUser
                    .save()
                    .then(user => {
                    // req.flash(
                    //     'success_msg',
                    //     'You are now registered and can log in'
                    // );
                    jwt.sign(
                      {
                         id : user.id ,
                         name : user.name ,
                         email : user.email
                      } ,
                      config.jwtSecret,
                      { expiresIn : 3600 } ,
                      (err , token) => {
                        if(err) {
                          console.log(err);
                          throw err;
                        }
                        const tokenData = token || "mock_token";
                        //console.log(tokenData);
              
                        return res.status(200).json({success : true , message : "Login successful !!!" , token : tokenData});
                      
                      }
                    )

                    //return res.status(200).json({ success : true , msg : "Register successfully"});
                    })
                    .catch(err => console.log(err));
                }
                // End Callback function
            );
          });
        }
    });
});

// Login
router.post('/login', (req, res, next) => {
    //console.log(req.body);

    // passport.authenticate('local', {
    //   successRedirect: '/dashboard',
    //   failureRedirect: '/users/login',
    //   failureFlash: true
    // })(req, res, next);
  
   //Test "Authenticate from react"
    passport.authenticate("local" , (err , user , info) => {
      //console.log("err : " + err);
      //console.log("user : " + user);
      //console.log("info : " + info.message);

      if(err) return next(err)
      if(!user) {
        return res.status(400).json({ success : false , message : info.message })
      }
      req.logIn(user , loginErr =>{
        if(loginErr) {
          return res.status(400).json({ success : false , message : "Invalid credentials !!!" })
        }
  
        console.log(process.env.ACCESS_TOKEN_SECRET);
        jwt.sign(
          {
             id : user.id ,
             name : user.name ,
             email : user.email
          } ,
          // config.jwtSecret,
          process.env.ACCESS_TOKEN_SECRET ,
          { expiresIn : 3600 } ,
          (err , token) => {
            if(err) {
              console.log(err);
              throw err;
            }
            const accessToken = token ;
            //console.log(tokenData);
  
            return res.status(200).json({success : true , message : "Login successful !!!" , token : accessToken});
          
          }
        )
        //console.log(token);
  
        //const token = null;
        // const tokenData = token || "mock_token";
        // console.log(tokenData);
        // return res.status(200).json({ success : true , message : "Login successful !!!" , token : tokenData })
      })
    })(req , res , next)
  });

// Logout
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
  });


module.exports = router