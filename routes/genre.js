const express = require('express');
const router = express.Router();

// Import Model
const Genre = require('../models/genre');

// Import Controllers
const genreController = require('../controllers/genre');

router.get( '/' , genreController.getGenres );

router.post('/' , genreController.addGenre );

router.put('/:_id' , genreController.updateGenre );

router.delete('/:_id' , genreController.deleteGenre );

module.exports = router ;