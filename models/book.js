const mongoose = require('mongoose');

// Book Schema
const bookSchema = mongoose.Schema({
    title : {
        type : String ,
        required : true
    } ,
    genre : {
        type : String ,
        required : true
    } ,
    description : {
        type : String ,
    } ,
    author : {
        type : String ,
        required : true
    } ,
    publisher : {
        type : String ,
        required : true
    } ,
    pages : {
        type : String ,
    } ,
    image_url : {
        type : String
    } ,
    buy_url : {
        type : String
    } ,
    create_date : {
        type : Date ,
        default : Date.now()
    } 

});

const Book = module.exports = mongoose.model('book' , bookSchema);

// Get Books
module.exports.getBooks = function(callback , limit){
    Book.find(callback).limit(limit);
}

// Get Book By ID
module.exports.getBookById = function(id  , callback){
    Book.findById(id , callback);
}

// Add Book
module.exports.addBook = function (bookData , callback){
    Book.create(bookData , callback);
}

// Update Book
module.exports.updateBook = function (id , bookData , options , callback){
    const query = { _id : id };
    const update = {
        title : bookData.title ,
        genre : bookData.genre ,
        description : bookData.description ,
        author : bookData.author ,
        pages : bookData.pages ,
        publisher : bookData.publisher ,
        image_url : bookData.image_url ,
        buy_url : bookData.buy_url ,
    };
    // console.log(id);
    // console.log(update);
    Book.findOneAndUpdate(query , update , options , callback);
}

// Delete Book
module.exports.deleteBook = function(id , callback){
    const query = {_id : id};
    Book.findOneAndRemove(query , callback);
}