const mongoose = require('mongoose');

// Genre Schema
const genreSchema = mongoose.Schema({
    name : {
        type : String ,
        required : true
    } ,
    create_date : {
        type : Date ,
        default : Date.now
    }

});

const Genre = module.exports = mongoose.model('genre' , genreSchema);

// Get Genres
module.exports.getGenres = function(callback , limit){
    Genre.find(callback).limit(limit);
}

// Add Genre
module.exports.addGenre = function(genreData , callback){
    Genre.create(genreData , callback);
}

// Update Genre
module.exports.updateGenre = function(id , genreData , options , callback){
    const query = {_id : id};
    //console.log(query);
    const update = {
        name : genreData.name
    }
    Genre.findOneAndUpdate(query , update , options , callback);
}

// Delete Genre
module.exports.deleteGenre = function(id , callback){
    const query = { _id : id }
    Genre.findOneAndRemove(query , callback);
}
