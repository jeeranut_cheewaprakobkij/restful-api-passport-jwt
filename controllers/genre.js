const Genre = require('../models/genre');

module.exports.getGenres = function(req , res){
    Genre.getGenres(function(err , genres){
        if(err){
            throw err;
        }
        res.json(genres);
    });
}

module.exports.addGenre = function(req , res) {
    const genre = req.body;
    Genre.addGenre(genre , function(err , genre){
        if(err){
            throw err;
        }
        res.json(genre);
    });
}

module.exports.updateGenre = function(req , res) {
    const _id = req.params._id;
    const genreData = req.body;
    Genre.updateGenre(_id , genreData , {} , function(err , data){
        if(err){
            throw err;
        }
        res.json(data);
    });
}

module.exports.deleteGenre = function(req , res){
    const _id = req.params._id;
    Genre.deleteGenre(_id , function(err , data){
        if(err){
            throw err;
        }
        res.json(data);
    })
}