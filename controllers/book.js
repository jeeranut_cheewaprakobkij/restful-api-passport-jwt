const Book = require('../models/book');

module.exports.getBook = function(req , res) {
    Book.getBooks(function(err , books){
        if(err){
            throw err;
        }
        res.json(books);
    });
}

module.exports.getBookById = function(req , res) {
    Book.getBookById(req.params._id , function(err , book){
        if(err){
            throw err;
        }
        res.json(book);
    });
}

module.exports.addBook = function(req , res) {
    const book = req.body;
    Book.addBook(book , function(err , book){
        if(err){
            throw err;
        }
        res.json(book);
    });
}


module.exports.updateBook = function(req , res) {
    const id = req.params._id ;
    const bookData = req.body ;
    Book.updateBook(id , bookData , {} , function(err , book){
        if(err){
            throw err;
        }
        res.json(book);
    });
}

module.exports.deleteBook = function(req , res) {
    const id = req.params._id ;
    Book.deleteBook(id , function(err , book){
        if(err){
            throw err;
        }
        res.json(book);
    });
}