const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('./config/keys');

const passport = require('passport');
const session = require('express-session');
const cors = require('cors');

const flash = require('connect-flash');

// Use CORS
app.use(cors());

// Parser body Middleware for req.body
app.use(express.json());

// Connect mongodb by mongoose
mongoose.set('useFindAndModify', false);

//console.log(config.mongoURI);
mongoose.connect(config.mongoURI);
const db = mongoose.connection;

// Config passport
require('./config/passport')(passport);

// Config Express-Session
app.use(
    session({
        secret : 'session_secret' ,
        resave : false ,
        saveUninitialized : true
    })
);

// Use Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// app.get('/' , function(req , res){
//     res.send("this is first place");
// });
//app.use(flash());

// Auth Routes
app.use('/api/user/v1' , require('./routes/user'));

// Routes
app.use('/api/genre/v1' , require('./routes/genre'));
app.use('/api/book/v1' , require('./routes/book'));

const port = process.env.port || 5000 ;
app.listen(port , () => console.log("Server is running on port:" + port));